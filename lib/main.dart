import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:redsocial/redsocialApp.dart';
import 'firebase_options.dart';

/*void main() {

  redsocialApp RedsocialApp = redsocialApp();
  runApp( RedsocialApp);
  initFB();
}

  void initFB() async{
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
  );
}
*/
void main() async{

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  redsocialApp RedsocialApp = redsocialApp();
  runApp( RedsocialApp);

}