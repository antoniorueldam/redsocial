
import 'package:flutter/material.dart';
import 'package:redsocial/Splash/SplashView.dart';
import 'Main/HomeView.dart';
import 'OnBoarding/LoginView.dart';

import 'OnBoarding/RegisterView.dart';

class redsocialApp extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    MaterialApp materialApp=MaterialApp(title: "Tu socialapp",
      routes: {
        '/loginview':(context) => LoginView(),
      '/registerview':(context) => RegisterView(),
        '/homeview':(context) => HomeView(),
        '/splashview':(context) => SplashView(),
      },
      initialRoute: '/loginview',
    );

    return materialApp;
  }

}